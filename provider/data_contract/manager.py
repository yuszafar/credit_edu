import requests

from models.managerdb import ManagerCreateIn
from exceptions.http_custom_exeption import HttpCustomExceptions
import urllib.parse

from settings import service_base_url


def create_manager(in_manager: ManagerCreateIn):

    in_manager.account.hash_password()
    account_url = urllib.parse.urljoin(service_base_url, 'account/create/')
    try:
        account_resp = requests.post(account_url, json=in_manager.account.dict())
    except:
        raise HttpCustomExceptions().service_connection_exception()

    if account_resp.status_code != 200:
        raise HttpCustomExceptions().service_error(account_resp)


    manager_req = in_manager.dict(exclude={'account'})

    manager_req.update({'account_id': account_resp.json()['id']})

    manager_url = urllib.parse.urljoin(service_base_url, 'manager/create/')
    try:
        manager_resp = requests.post(url=manager_url, json=manager_req)
    except:
        raise HttpCustomExceptions().service_connection_exception()

    if account_resp.status_code != 200:
        raise HttpCustomExceptions().service_error(manager_resp)

    return manager_resp.json()