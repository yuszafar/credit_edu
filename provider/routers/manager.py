from fastapi import APIRouter
from fastapi.responses import JSONResponse

from models.managerdb import *
from settings import error_responses
import requests
from data_contract.manager import create_manager
api_router = APIRouter()


@api_router.post("/create/", response_model=ManagerShortOut, responses=error_responses)
def account_create(in_manager: ManagerCreateIn):
    out_manager = create_manager(in_manager)

    return JSONResponse(content=out_manager)
