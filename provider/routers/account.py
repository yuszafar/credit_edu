from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from data_contract.account import *

from models.accountdb import *
from settings import error_responses, ACCESS_TOKEN_EXPIRE_MINUTES

api_router = APIRouter()

@api_router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    role = get_role(user.id)
    access_token = create_access_token(
        data={"username": user.username, "role": role}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@api_router.get("/users/me/", response_model=AccountDetailOut)
def read_users_me(current_user: AccountDetailOut = Depends(get_current_active_user)):
    return current_user

