from fastapi import APIRouter
from routers import manager, account
api_router = APIRouter()

api_router.include_router(manager.api_router, prefix="/manager", tags=["manager"])
api_router.include_router(account.api_router, prefix="/account", tags=["account"])