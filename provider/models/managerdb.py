from datetime import datetime
from typing import List

from pydantic import BaseModel

from models.accountdb import AccountCreateIn


class ManagerCreateIn(BaseModel):
    first_name: str
    last_name: str
    father_name: str = None
    account: AccountCreateIn


class ManagerUpdateIn(BaseModel):
    first_name: str = None
    last_name: str = None
    father_name: str = None
    account_id: int = None


class ManagerShortOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    father_name: str = None
    username: str


class ManagerDetailOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    father_name: str = None
    account_id: int
    created: datetime
    updated: datetime = None


class ManagerList(BaseModel):
    list: List[ManagerDetailOut]



