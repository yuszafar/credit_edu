from datetime import datetime, time, timedelta
from typing import List
from pydantic import BaseModel



class EventTimeCreateIn(BaseModel):
    number: str
    start_time: time
    end_time: time


class EventTimeUpdateIn(BaseModel):
    number: str = None
    start_time: time = None
    end_time: time = None


class EventTimeShortOut(BaseModel):
    id: int
    number: str
    start_time: time
    end_time: time


class EventTimeDetailOut(BaseModel):
    id: int
    number: str
    start_time: time
    end_time: time
    created: datetime
    updated: datetime = None


class EventTimeList(BaseModel):
    list: List[EventTimeDetailOut]



