from datetime import datetime
from typing import List

from pydantic import BaseModel




class AccountCreateIn(BaseModel):
    username: str
    password: str


class AccountUpdateIn(BaseModel):
    username: str = None
    password: str = None


class AccountShortOut(BaseModel):
    id: int
    username: str


class AccountDetailOut(BaseModel):
    id: int
    username: str
    password: str
    created: datetime
    updated: datetime = None


class AccountList(BaseModel):
    list: List[AccountDetailOut]



