from datetime import datetime
from typing import List
from pydantic import BaseModel




class BuildingCreateIn(BaseModel):
    name: str
    address: str
    size: int


class BuildingUpdateIn(BaseModel):
    name: str = None
    address: str = None
    size: int = None


class BuildingShortOut(BaseModel):
    id: int
    name: str
    address: str
    size: int


class BuildingDetailOut(BaseModel):
    id: int
    name: str
    address: str
    size: int
    created: datetime
    updated: datetime = None


class BuildingList(BaseModel):
    list: List[BuildingDetailOut]


