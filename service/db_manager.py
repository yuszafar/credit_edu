import psycopg2
import settings
import os

conn = None


def connect():
    global conn
    try:
        conn = psycopg2.connect(dbname=settings.DB_NAME,
                                user=settings.DB_USER,
                                password=settings.DB_PASSWORD,
                                host=settings.DB_HOST,
                                port=settings.DB_PORT)
    except psycopg2.DatabaseError as e:
        print(e)


def get_db():
    if not conn:
        connect()
    return conn



def db_init():
    get_db()
    print('init tables:')
    tables_path = os.path.join(settings.DB_INIT_PATH, 'tables')
    for filename in os.listdir(tables_path):
        table = os.path.abspath(os.path.join(tables_path, filename))

        with conn.cursor() as cur:
            try:
                cur.execute(open(table, "r").read())
            except psycopg2.Error as e:
                print(filename, '-- error')
            else:
                print(filename, '-- success')

    print('init functions:')
    functions_path = os.path.join(settings.DB_INIT_PATH, 'tables')
    for filename in os.listdir(functions_path):
        table = os.path.abspath(os.path.join(functions_path, filename))

        with conn.cursor() as cur:
            try:
                cur.execute(open(table, "r").read())
            except psycopg2.Error as e:
                print(filename, '-- error')
            else:
                print(filename, '-- success')

