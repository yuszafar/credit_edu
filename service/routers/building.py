from fastapi import APIRouter

from data_contract.building import *
from settings import error_responses

api_router = APIRouter()


@api_router.post("/create/", response_model=BuildingShortOut, responses=error_responses)
def building_create(in_building: BuildingCreateIn):
    return BuildingDB().add(in_building)


@api_router.put("/{id}", response_model=BuildingShortOut, responses=error_responses)
def building_update(id: int, in_building: BuildingUpdateIn):
    return BuildingDB().update(id, in_building)


@api_router.delete("/{id}", responses=error_responses)
def building_delete(id: int):
    return BuildingDB().delete(id)


@api_router.get("/{id}", response_model=BuildingDetailOut, responses=error_responses)
def building_get(id: int):
    return BuildingDB().get(id)


@api_router.get("/get_list/", response_model=BuildingList, responses=error_responses)
def building_get_list(id: int = None, name: str = None, address: str = None, size: int = None):
    return BuildingDB().get_list(id, name, address, size)
