from fastapi import APIRouter
from data_contract.account import *
from settings import error_responses

api_router = APIRouter()


@api_router.post("/create/", response_model=AccountShortOut, responses=error_responses)
def account_create(in_account: AccountCreateIn):
    return AccountDB().add(in_account)


@api_router.put("/{id}", response_model=AccountShortOut, responses=error_responses)
def account_update(id: int, in_account: AccountUpdateIn):
    return AccountDB().update(id, in_account)


@api_router.delete("/{id}", responses=error_responses)
def account_delete(id: int):
    return AccountDB().delete(id)


@api_router.get("/{id}", response_model=AccountDetailOut, responses=error_responses)
def account_get(id: int):
    return AccountDB().get(id)


@api_router.get("/get_list/", response_model=AccountList, responses=error_responses)
def account_get_list(id: int = None, username: str = None):
    return AccountDB().get_list(id, username)
