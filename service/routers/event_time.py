from fastapi import APIRouter

from data_contract.event_time import *
from settings import error_responses

api_router = APIRouter()


@api_router.post("/create/", response_model=EventTimeShortOut, responses=error_responses)
def event_time_create(in_event_time: EventTimeCreateIn):
    return EventTimeDB().add(in_event_time)


@api_router.put("/{id}", response_model=EventTimeShortOut, responses=error_responses)
def event_time_update(id: int, in_event_time: EventTimeUpdateIn):
    return EventTimeDB().update(id, in_event_time)


@api_router.delete("/{id}", responses=error_responses)
def event_time_delete(id: int):
    return EventTimeDB().delete(id)


@api_router.get("/{id}", response_model=EventTimeDetailOut, responses=error_responses)
def event_time_get(id: int):
    return EventTimeDB().get(id)


@api_router.get("/get_list/", response_model=EventTimeList, responses=error_responses)
def event_time_get_list(id: int = None, number: str = None, start_time: time = None, end_time: time = None):
    return EventTimeDB().get_list(id, number, start_time, end_time)
