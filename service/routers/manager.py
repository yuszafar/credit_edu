from fastapi import APIRouter

from data_contract.manager import *
from settings import error_responses

api_router = APIRouter()


@api_router.post("/create/", response_model=ManagerShortOut, responses=error_responses)
def manager_create(in_manager: ManagerCreateIn):
    return ManagerDB().add(in_manager)


@api_router.put("/{id}", response_model=ManagerShortOut, responses=error_responses)
def manager_update(id: int, in_manager: ManagerUpdateIn):
    return ManagerDB().update(id, in_manager)


@api_router.delete("/{id}", responses=error_responses)
def manager_delete(id: int):
    return ManagerDB().delete(id)


@api_router.get("/{id}", response_model=ManagerDetailOut, responses=error_responses)
def manager_get(id: int):
    return ManagerDB().get(id)


@api_router.get("/get_list/", response_model=ManagerList, responses=error_responses)
def manager_get_list(id: int = None, first_name: str = None, last_name: str = None, father_name: str = None,
                     account_id: int = None):
    return ManagerDB().get_list(id, first_name, last_name, father_name, account_id)
