
import psycopg2
from fastapi import HTTPException
from models.event_time import *

from db_manager import get_db


class EventTimeDB:

    def __init__(self):
        self.conn = get_db()

    def add(self, in_event_time: EventTimeCreateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('event_time_add', [in_event_time.number, in_event_time.start_time, in_event_time.end_time])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    in_event_time = cur.fetchone()
                    event_time_created = EventTimeShortOut(id=in_event_time[0],
                                                           number=in_event_time[1],
                                                           start_time=in_event_time[2],
                                                           end_time=in_event_time[3])
                    return event_time_created

    def update(self, in_id, in_event_time: EventTimeUpdateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('event_time_update', [in_id, in_event_time.number, in_event_time.start_time,
                                                       in_event_time.end_time])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)

                else:
                    in_event_time = cur.fetchone()
                    event_time_updated = EventTimeShortOut(id=in_event_time[0],
                                                           number=in_event_time[1],
                                                           start_time=in_event_time[2],
                                                           end_time=in_event_time[3])
                    return event_time_updated

    def delete(self, in_id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('event_time_delete', [in_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    return True

    def get(self, in_id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('event_time_get', [in_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    event_time = cur.fetchone()
                    event_time_get = EventTimeDetailOut(id=event_time[0],
                                                            number=event_time[1],
                                                            start_time=event_time[2],
                                                            end_time=event_time[3],
                                                            created=event_time[4],
                                                            updated=event_time[5])
                    return event_time_get

    def get_list(self, id=None, number=None, start_date=None, end_date=None):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('event_time_get_list', [id, number, start_date, end_date])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    event_time_list = cur.fetchall()
                    get_event_time_list = []
                    for event_time in event_time_list:
                        get_event_time_list.append(EventTimeDetailOut(id=event_time[0],
                                                                      number=event_time[1],
                                                                      start_time=event_time[2],
                                                                      end_time=event_time[3],
                                                                      created=event_time[4],
                                                                      updated=event_time[5]))

                    return EventTimeList(list=get_event_time_list)
