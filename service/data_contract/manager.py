import psycopg2
from fastapi import HTTPException
from db_manager import get_db
from models.managerdb import *


class ManagerDB:

    def __init__(self):
        self.conn = get_db()

    def add(self, in_manager: ManagerCreateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('manager_add', [in_manager.first_name, in_manager.last_name, in_manager.father_name,
                                                 in_manager.account_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    manager = cur.fetchone()
                    created_manager = ManagerShortOut(id=manager[0],
                                                      first_name=manager[1],
                                                      last_name=manager[2],
                                                      father_name=manager[3],
                                                      account_id=manager[4])
                    return created_manager

    def update(self, in_id, in_manager: ManagerUpdateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('manager_update', [in_id, in_manager.first_name, in_manager.last_name,
                                                    in_manager.father_name, in_manager.account_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    manager = cur.fetchone()
                    updated_manager = ManagerShortOut(id=manager[0],
                                                      first_name=manager[1],
                                                      last_name=manager[2],
                                                      father_name=manager[3],
                                                      account_id=manager[4])
                    return updated_manager

    def delete(self, id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('manager_delete', [id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    return True

    def get(self, id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('manager_get', [id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    manager = cur.fetchone()
                    get_manager = ManagerDetailOut(id=manager[0],
                                                   first_name=manager[1],
                                                   last_name=manager[2],
                                                   father_name=manager[3],
                                                   account_id=manager[4],
                                                   created=manager[5],
                                                   updated=manager[6])
                    return get_manager

    def get_list(self, id=None, first_name=None, last_name=None, father_name=None, account_id=None):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('manager_get_list', [id, first_name, last_name, father_name, account_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    manager_list = cur.fetchall()
                    get_manager_list = []
                    for manager in manager_list:
                        get_manager_list.append(ManagerDetailOut(id=manager[0],
                                                   first_name=manager[1],
                                                   last_name=manager[2],
                                                   father_name=manager[3],
                                                   account_id=manager[4],
                                                   created=manager[5],
                                                   updated=manager[6]))

                    return ManagerList(list=get_manager_list)
