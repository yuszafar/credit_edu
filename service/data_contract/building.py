import psycopg2
from fastapi import HTTPException
from models.buildingdb import *

from db_manager import get_db



class BuildingDB:

    def __init__(self):
        self.conn = get_db()

    def add(self, in_building: BuildingCreateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('building_add', [in_building.name, in_building.address, in_building.size])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    in_building = cur.fetchone()
                    created_building = BuildingShortOut(id=in_building[0],
                                                        name=in_building[1],
                                                        address=in_building[2],
                                                        size=in_building[3])
                    return created_building

    def update(self, in_id, in_building: BuildingUpdateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('building_update', [in_id, in_building.name, in_building.address, in_building.size])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    in_building = cur.fetchone()
                    updated_building = BuildingShortOut(id=in_building[0],
                                                        name=in_building[1],
                                                        address=in_building[2],
                                                        size=in_building[3])
                    return updated_building

    def delete(self, in_id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('building_delete', [in_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    return True

    def get(self, in_id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('building_get', [in_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    building = cur.fetchone()
                    get_building = BuildingDetailOut(id=building[0],
                                                     name=building[1],
                                                     address=building[2],
                                                     size=building[3],
                                                     created=building[4],
                                                     updated=building[5])
                    return get_building

    def get_list(self, in_id=None, name=None, address=None, size=None):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('building_get_list', [in_id, name, address, size])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    building_list = cur.fetchall()
                    get_building_list = []
                    for building in building_list:
                        get_building_list.append(BuildingDetailOut(id=building[0],
                                                     name=building[1],
                                                     address=building[2],
                                                     size=building[3],
                                                     created=building[4],
                                                     updated=building[5]))

                    return BuildingList(list=get_building_list)
