import psycopg2
from fastapi import HTTPException
from models.accountdb import *

from db_manager import get_db


class AccountDB:

    def __init__(self):
        self.conn = get_db()

    def add(self, in_account: AccountCreateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('account_add', [in_account.username, in_account.password])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    user = cur.fetchone()
                    created_user = AccountShortOut(id=user[0], username=user[1])
                    return created_user

    def update(self, in_id, in_account: AccountUpdateIn):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('account_update', [in_id, in_account.username, in_account.password])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    user = cur.fetchone()
                    updated_user = AccountShortOut(id=user[0], username=user[1])
                    return updated_user

    def delete(self, in_id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('account_delete', [in_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    return True

    def get(self, in_id):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('account_get', [in_id])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:
                    user = cur.fetchone()
                    get_user = AccountDetailOut(id=user[0], username=user[1], password=user[2], created=user[3],
                                                updated=user[4])
                    return get_user

    def get_list(self, in_id=None, username=None):
        with self.conn:
            with self.conn.cursor() as cur:
                try:
                    cur.callproc('account_get_list', [in_id, username])
                except psycopg2.Error as e:
                    raise HTTPException(status_code=400, detail=e.diag.message_primary + "; " + e.diag.message_hint)
                else:

                    user_list = cur.fetchall()
                    get_user_list = []
                    for user in user_list:
                        get_user_list.append(
                            AccountDetailOut(id=user[0], username=user[1], password=user[2], created=user[3],
                                             updated=user[4]))
                    return AccountList(list=get_user_list)
