from fastapi import APIRouter
from routers import account, building, event_time, manager
api_router = APIRouter()

api_router.include_router(account.api_router, prefix="/account", tags=["account"])
api_router.include_router(building.api_router, prefix="/building", tags=["building"])
api_router.include_router(event_time.api_router, prefix="/event_time", tags=["event_time"])
api_router.include_router(manager.api_router, prefix="/manager", tags=["manager"])