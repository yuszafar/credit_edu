from fastapi import FastAPI
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

import settings
from db_manager import db_init
from router import api_router

import sentry_sdk


sentry_sdk.init(dsn=settings.SENTRY_KEY)

app = FastAPI()
app.add_middleware(SentryAsgiMiddleware)
app.include_router(api_router, prefix='/api/v1')