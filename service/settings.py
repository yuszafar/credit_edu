import os
from dotenv import load_dotenv
load_dotenv()

error_responses = {
    400: {
        "description": "Bad request",
        "content": {
            "application/json": {
                "example": {"detail": "Error --> {code} {function_name} params [params]; {hint detail}"}
            }
        }
    }
}


# POSTGRES DATABASE

DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')

# SENTRY
SENTRY_KEY = os.getenv('SENTRY_KEY')

# DB FOLDER
DB_INIT_PATH = os.getenv('DB_INIT_PATH')