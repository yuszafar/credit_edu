from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.templating import Jinja2Templates

from middlewares.auth import BasicAuthBackend, JWTAuthBackend

templates = Jinja2Templates(directory='templates')

middleware = [
    Middleware(AuthenticationMiddleware, backend=JWTAuthBackend())
]

controller1_base_url = 'http://127.0.0.1:8001/api/v1/'