from starlette.routing import Route, Mount

from controllers.index import homepage
from starlette.staticfiles import StaticFiles
from controllers.account import account_routes
from controllers.account import Login

routes = [
    Route('/', endpoint=homepage),
    Mount('/account', routes=account_routes),
    Mount('/static', StaticFiles(directory='static'), name='static')
]