import requests
from starlette.endpoints import HTTPEndpoint
from starlette.routing import Route
import urllib.parse

from settings import templates, controller1_base_url


class Login(HTTPEndpoint):
    async def get(self, request):
        return templates.TemplateResponse('login.html', {'request': request})

    async def post(self, request):
        data = await request.form()
        if data.get('username') and data.get('password'):
            login_req_url = urllib.parse.urljoin(controller1_base_url, 'account/token')
            login_resp = requests.post(login_req_url,
                                 data={'username': data.get('username'),
                                       'password': data.get('password')})
            if login_resp.status_code != 200:
                return templates.TemplateResponse('login.html', {'request': request, 'login_error': True})

            resp = templates.TemplateResponse('login.html', {'request': request})
            resp.set_cookie(key='token', value=login_resp.json().get('access_token'))
        return resp


account_routes = [
    Route('/login', endpoint=Login),
]
