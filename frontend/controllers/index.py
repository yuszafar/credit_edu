from settings import templates


async def homepage(request):
    print(request.cookies)
    response = templates.TemplateResponse('index.html', {'request': request, 'user': request.user})
    return response

