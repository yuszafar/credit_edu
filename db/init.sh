cat public/functions/*.sql > functions.sql
cat public/tables/*.sql > tables.sql
#cat public/sequences/*.sql > sequences.sql
psql -U $POSTGRES_USER -d $POSTGRES_DB -a -f tables.sql
psql -U $POSTGRES_USER -d $POSTGRES_DB -a -f functions.sql
#psql -U $POSTGRES_USER -d $POSTGRES_DB -a -f sequences.sql