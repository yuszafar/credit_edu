/*****************
* File name: account_delete.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-28 First init
*************/


CREATE OR REPLACE FUNCTION account_delete(in_account_id BIGINT DEFAULT NULL)
    returns VOID
as
$$
DECLARE
    FUNC_NAME TEXT := 'account_delete';
    ERR_CODE  INT  := 1002;

BEGIN
    IF in_account_id IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_account_id
            USING HINT = 'account_id cannot be null';
    END IF;

    IF NOT exists(SELECT 1 FROM accounts WHERE id_account = in_account_id) OR
       exists(SELECT 1 FROM accounts WHERE id_account = in_account_id AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_account_id
            USING HINT = 'account not exists';
    end if;

    UPDATE accounts SET is_delete = TRUE, exp_date = now() WHERE id_account = in_account_id;


end;
$$
    LANGUAGE 'plpgsql';

