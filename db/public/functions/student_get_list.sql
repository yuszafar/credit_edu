/*****************
* File name: student_get_list.sql
* Author: Zafar
* Date: 2021-05-05
* Version: 1.0
*************
* Changelog
* 2021-05-05 First init
*************/


CREATE OR REPLACE FUNCTION student_get_list(in_id_student BIGINT DEFAULT NULL, in_first_name TEXT DEFAULT NULL,
                                             in_last_name TEXT DEFAULT NULL, in_father_name TEXT DEFAULT NULL,
                                             in_account_id INT DEFAULT NULL)
     returns TABLE
            (
                out_id_student  BIGINT,
                out_first_name  TEXT,
                out_last_name   TEXT,
                out_father_name TEXT,
                out_account_id  INT,
                out_reg_date    TIMESTAMP WITH TIME ZONE,
                out_mod_date    TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'student_get_list';
    ERR_CODE  INT  := 5005;

BEGIN
    return QUERY SELECT id_student,
                        first_name,
                        last_name,
                        father_name,
                        user_id,
                        reg_date,
                        mod_date
                 FROM students

                 where (in_id_student IS NULL OR id_student = in_id_student)
                   AND (in_first_name IS NULL OR first_name = in_first_name)
                   AND (in_last_name IS NULL OR last_name = in_last_name)
                   AND (in_father_name IS NULL OR father_name = in_father_name)
                   AND (in_account_id IS NULL OR user_id = in_account_id)
                   AND is_delete = FALSE;

end;
$$
    LANGUAGE 'plpgsql';

