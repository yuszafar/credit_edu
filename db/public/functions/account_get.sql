/*****************
* File name: account_get.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-28 First init
* 2021-05-02 change return params
* 2021-05-13 added out_password
*************/


CREATE OR REPLACE FUNCTION account_get(in_account_id BIGINT DEFAULT NULL)
    returns TABLE
            (
                out_id_user   BIGINT,
                out_username  TEXT,
                out_password  TEXT,
                out_reg_date  TIMESTAMP WITH TIME ZONE,
                out_mod_date  TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'account_get';
    ERR_CODE  INT  := 1004;

BEGIN
    IF in_account_id IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_account_id
            USING HINT = 'account_id cannot be null';
    END IF;

    IF NOT exists(SELECT 1 FROM accounts WHERE id_account = in_account_id) OR
       exists(SELECT 1 FROM accounts WHERE id_account = in_account_id AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_account_id
            USING HINT = 'account not exists';
    end if;

    RETURN QUERY SELECT id_account, username, password, reg_date, mod_date
                 FROM accounts
                 WHERE id_account = in_account_id;

end;
$$
    LANGUAGE 'plpgsql';

