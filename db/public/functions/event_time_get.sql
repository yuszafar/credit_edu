/*****************
* File name: event_time_get.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION event_time_get(in_id_event_time BIGINT DEFAULT NULL)
    returns TABLE
            (
                out_id_event_time BIGINT,
                out_number        TEXT,
                out_start_time    TIME,
                out_end_time      TIME,
                out_reg_date      TIMESTAMP WITH TIME ZONE,
                out_mod_date      TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'event_time_get';
    ERR_CODE  INT  := 3004;

BEGIN
    IF in_id_event_time IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_event_time
            USING HINT = 'id_event_time cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM event_times WHERE id_event_time = in_id_event_time) OR
       EXISTS(SELECT 1 FROM event_times WHERE id_event_time = in_id_event_time AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_event_time
            USING HINT = 'id_event_time not exists';
    END IF;

    RETURN QUERY SELECT id_event_time, number, start_time, end_time, reg_date, mod_date
                 FROM event_times
                 WHERE id_event_time = in_id_event_time;


end;
$$
    LANGUAGE 'plpgsql';

