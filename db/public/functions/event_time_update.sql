/*****************
* File name: event_time_update.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION event_time_update(in_id_event_time BIGINT DEFAULT NULL, in_number TEXT DEFAULT NULL,
                                               in_start_time TIME DEFAULT NULL, in_end_time TIME DEFAULT NULL)
    returns TABLE
            (
                out_id_event_time  BIGINT,
                out_number TEXT,
                out_start_time TIME,
                out_end_time TIME
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'event_time_update';
    ERR_CODE  INT  := 3003;

BEGIN
    IF in_id_event_time IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %', ERR_CODE, FUNC_NAME, in_id_event_time, in_number, in_start_time, in_end_time
            USING HINT = 'id_event_time cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM event_times WHERE id_event_time=in_id_event_time) OR
       EXISTS(SELECT 1 FROM event_times WHERE id_event_time = in_id_event_time AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %', ERR_CODE, FUNC_NAME, in_id_event_time, in_number, in_start_time, in_end_time
            USING HINT = 'event_time not exists';
    END IF;


    UPDATE event_times
    SET number=COALESCE(in_number, number),
        start_time=COALESCE(in_start_time, start_time),
        end_time=COALESCE(in_end_time, end_time),
        mod_date = now()
    WHERE id_event_time = in_id_event_time;

    RETURN QUERY SELECT id_event_time, number, start_time, end_time
                 FROM event_times
                 WHERE id_event_time = in_id_event_time;




end;
$$
    LANGUAGE 'plpgsql';

