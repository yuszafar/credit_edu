/*****************
* File name: building_update.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION building_update(in_id_building BIGINT DEFAULT NULL, in_name TEXT DEFAULT NULL, in_address TEXT DEFAULT NULL, in_size INT DEFAULT NULL)
    returns TABLE
            (
                out_id_building  BIGINT,
                out_name TEXT,
                out_address TEXT,
                out_size INT
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'building_update';
    ERR_CODE  INT  := 2003;

BEGIN
    IF in_id_building IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %', ERR_CODE, FUNC_NAME, in_id_building, in_name, in_address, in_size
            USING HINT = 'id_building cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM buildings WHERE id_building=in_id_building) OR
       EXISTS(SELECT 1 FROM buildings WHERE id_building = in_id_building AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %', ERR_CODE, FUNC_NAME, in_id_building, in_name, in_address, in_size
            USING HINT = 'building not exists';
    END IF;


    UPDATE buildings
    SET name=COALESCE(in_name, name),
        address=COALESCE(in_address, address),
        size=COALESCE(in_size, size),
        mod_date = now()
    WHERE id_building = in_id_building;

    RETURN QUERY SELECT id_building, name, address, size
                 FROM buildings
                 WHERE id_building = in_id_building;




end;
$$
    LANGUAGE 'plpgsql';

