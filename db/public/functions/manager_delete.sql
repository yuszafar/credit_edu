/*****************
* File name: manager_delete.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION manager_delete(in_id_manager BIGINT DEFAULT NULL)
    returns VOID
as
$$
DECLARE
    FUNC_NAME TEXT := 'manager_delete';
    ERR_CODE  INT  := 4002;
    deleted_account INT;

BEGIN
    IF in_id_manager IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_manager
            USING HINT = 'in_id_manager cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM managers WHERE id_manager=in_id_manager) OR
       EXISTS(SELECT 1 FROM managers WHERE id_manager = in_id_manager AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_manager
            USING HINT = 'manager not exists';
    END IF;

    SELECT user_id INTO deleted_account FROM managers WHERE id_manager = in_id_manager;
    PERFORM account_delete(deleted_account);

    UPDATE managers SET is_delete = TRUE, exp_date = now() WHERE id_manager = in_id_manager;




end;
$$
    LANGUAGE 'plpgsql';

