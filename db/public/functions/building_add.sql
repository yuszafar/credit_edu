/*****************
* File name: building_add.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION building_add(in_name TEXT DEFAULT NULL, in_address TEXT DEFAULT NULL, in_size INT DEFAULT NULL)
    returns TABLE
            (
                out_id_building  BIGINT,
                out_name TEXT,
                out_address TEXT,
                out_size INT
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'buildings_add';
    ERR_CODE  INT  := 2001;

BEGIN
    IF in_name IS NULL OR in_address IS NULL OR in_size IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %', ERR_CODE, FUNC_NAME, in_name, in_address, in_size
            USING HINT = 'name or address or size cannot be null';
    END IF;


    RETURN QUERY INSERT INTO buildings (name, address, size) VALUES (in_name, in_address, in_size)
        RETURNING id_building, name, address, size;


end;
$$
    LANGUAGE 'plpgsql';

