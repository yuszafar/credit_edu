/*****************
* File name: building_get_list.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
* 2021-05-02 mode_date and reg_date added
*************/


CREATE OR REPLACE FUNCTION building_get_list(in_id_building BIGINT DEFAULT NULL, in_name TEXT DEFAULT NULL,
                                             in_address TEXT DEFAULT NULL, in_size INT DEFAULT NULL)
    returns TABLE
            (
                out_id_building BIGINT,
                out_name        TEXT,
                out_address     TEXT,
                out_size        INT,
                out_reg_date    TIMESTAMP WITH TIME ZONE,
                out_mod_date    TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'building_get_list';
    ERR_CODE  INT  := 2005;

BEGIN
    return QUERY SELECT id_building,
                        name,
                        address,
                        size,
                        reg_date,
                        mod_date
                 FROM buildings

                 where (in_id_building IS NULL OR id_building = in_id_building)
                   AND (in_name IS NULL OR name = in_name)
                   AND (in_address IS NULL OR address = in_address)
                   AND (in_size IS NULL OR size = in_size)
                   AND is_delete = FALSE;

end;
$$
    LANGUAGE 'plpgsql';

