/*****************
* File name: manager_get.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION manager_get(in_id_manager BIGINT DEFAULT NULL)
    returns TABLE
            (
                out_id_manager  BIGINT,
                out_first_name  TEXT,
                out_last_name   TEXT,
                out_father_name TEXT,
                out_account_id  INT,
                out_reg_date    TIMESTAMP WITH TIME ZONE,
                out_mod_date    TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'manager_get';
    ERR_CODE  INT  := 4004;

BEGIN
    IF in_id_manager IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_manager
            USING HINT = 'id_manager cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM managers WHERE id_manager=in_id_manager) OR
       EXISTS(SELECT 1 FROM managers WHERE id_manager = in_id_manager AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_manager
            USING HINT = 'manager not exists';
    END IF;

    RETURN QUERY SELECT id_manager, first_name, last_name, father_name, user_id, reg_date, mod_date
                 FROM managers
                 WHERE id_manager = in_id_manager;




end;
$$
    LANGUAGE 'plpgsql';

