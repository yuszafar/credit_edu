/*****************
* File name: manager_update.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
* 2021-05-05 add validation for in_account_id init
*************/


CREATE OR REPLACE FUNCTION manager_update(in_id_manager BIGINT DEFAULT NULL, in_first_name TEXT DEFAULT NULL,
                                          in_last_name TEXT DEFAULT NULL, in_father_name TEXT DEFAULT NULL,
                                          in_account_id INT DEFAULT NULL)
    returns TABLE
            (
                out_id_manager  BIGINT,
                out_first_name  TEXT,
                out_last_name   TEXT,
                out_father_name TEXT,
                out_account_id  INT
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'manager_update';
    ERR_CODE  INT  := 4003;

BEGIN
    IF in_id_manager IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %, %', ERR_CODE, FUNC_NAME, in_id_manager, in_first_name, in_last_name, in_father_name, in_account_id
            USING HINT = 'id_manager cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM managers WHERE id_manager = in_id_manager) OR
       EXISTS(SELECT 1 FROM managers WHERE id_manager = in_id_manager AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %, %', ERR_CODE, FUNC_NAME, in_id_manager, in_first_name, in_last_name, in_father_name, in_account_id
            USING HINT = 'manager not exists';
    END IF;

    IF in_account_id IS NOT NULL AND (NOT EXISTS(SELECT 1 FROM accounts WHERE id_account = in_account_id) OR
                                      EXISTS(
                                              SELECT 1 FROM accounts WHERE id_account = in_account_id AND is_delete = TRUE)) THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %, %', ERR_CODE, FUNC_NAME, in_id_manager, in_first_name, in_last_name, in_father_name, in_account_id
            USING HINT = 'account not exists';
    END IF;

    IF in_account_id IS NOT NULL AND EXISTS(SELECT 1 FROM managers WHERE user_id = in_account_id) OR
       EXISTS(SELECT 1 FROM students WHERE user_id = in_account_id) OR
       EXISTS(SELECT 1 FROM teachers WHERE user_id = in_account_id) THEN

        RAISE EXCEPTION 'Error --> % % params %, %, %, %, %', ERR_CODE, FUNC_NAME, in_id_manager, in_first_name, in_last_name, in_father_name, in_account_id
            USING HINT = 'a manager with the account_id already exists';
    END IF;

    UPDATE managers
    SET first_name=COALESCE(in_first_name, first_name),
        last_name=COALESCE(in_last_name, last_name),
        father_name=COALESCE(in_father_name, father_name),
        user_id=COALESCE(in_account_id, user_id),
        mod_date = now()
    WHERE id_manager = in_id_manager;

    RETURN QUERY SELECT id_manager, first_name, last_name, father_name, user_id
                 FROM managers
                 WHERE id_manager = in_id_manager;


end;
$$
    LANGUAGE 'plpgsql';

