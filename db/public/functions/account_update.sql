/*****************
* File name: account_update.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-28 First init
* 2021-05-02 change return params
*************/


CREATE OR REPLACE FUNCTION account_update(in_account_id BIGINT DEFAULT NULL, in_username TEXT DEFAULT NULL,
                                          in_password TEXT DEFAULT NULL)
    returns TABLE
            (
                out_id_user   BIGINT,
                out_username  TEXT,
                out_reg_date  TIMESTAMP WITH TIME ZONE,
                out_mod_date  TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'account_update';
    ERR_CODE  INT  := 1003;

BEGIN
    IF in_account_id IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_account_id
            USING HINT = 'account_id cannot be null';
    END IF;

    IF NOT exists(SELECT 1 FROM accounts WHERE id_account = in_account_id) OR
       exists(SELECT 1 FROM accounts WHERE id_account = in_account_id AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_account_id
            USING HINT = 'account not exists';
    end if;

    IF exists(SELECT 1 FROM accounts WHERE username = in_username) THEN
        RAISE EXCEPTION 'Error --> % % params %, %', ERR_CODE, FUNC_NAME, in_username, in_password
            USING HINT = 'username already exists';
    end if;

    UPDATE accounts
    SET username=COALESCE(in_username, username),
        password=COALESCE(in_password, password),
        mod_date = now()
    WHERE id_account = in_account_id;

    RETURN QUERY SELECT id_account, username, reg_date, mod_date
                 FROM accounts
                 WHERE id_account = in_account_id;

end;
$$
    LANGUAGE 'plpgsql';

