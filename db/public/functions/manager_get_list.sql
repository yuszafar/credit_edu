/*****************
* File name: manager_get_list.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
* 2021-05-02 mode_date and reg_date added
*************/


CREATE OR REPLACE FUNCTION manager_get_list(in_id_manager BIGINT DEFAULT NULL, in_first_name TEXT DEFAULT NULL,
                                             in_last_name TEXT DEFAULT NULL, in_father_name TEXT DEFAULT NULL,
                                             in_account_id INT DEFAULT NULL)
     returns TABLE
            (
                out_id_manager  BIGINT,
                out_first_name  TEXT,
                out_last_name   TEXT,
                out_father_name TEXT,
                out_account_id  INT,
                out_reg_date    TIMESTAMP WITH TIME ZONE,
                out_mod_date    TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'manager_get_list';
    ERR_CODE  INT  := 4005;

BEGIN
    return QUERY SELECT id_manager,
                        first_name,
                        last_name,
                        father_name,
                        user_id,
                        reg_date,
                        mod_date
                 FROM managers

                 where (in_id_manager IS NULL OR id_manager = in_id_manager)
                   AND (in_first_name IS NULL OR first_name = in_first_name)
                   AND (in_last_name IS NULL OR last_name = in_last_name)
                   AND (in_father_name IS NULL OR father_name = in_father_name)
                   AND (in_account_id IS NULL OR user_id = in_account_id)
                   AND is_delete = FALSE;

end;
$$
    LANGUAGE 'plpgsql';

