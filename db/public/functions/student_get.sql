/*****************
* File name: student_get.sql
* Author: Zafar
* Date: 2021-05-05
* Version: 1.0
*************
* Changelog
* 2021-05-05 First init
*************/


CREATE OR REPLACE FUNCTION student_get(in_id_student BIGINT DEFAULT NULL)
    returns TABLE
            (
                out_id_student  BIGINT,
                out_first_name  TEXT,
                out_last_name   TEXT,
                out_father_name TEXT,
                out_account_id  INT,
                out_reg_date    TIMESTAMP WITH TIME ZONE,
                out_mod_date    TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'student_get';
    ERR_CODE  INT  := 5005;

BEGIN
    IF in_id_student IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_student
            USING HINT = 'id_student cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM students WHERE id_student=in_id_student) OR
       EXISTS(SELECT 1 FROM students WHERE id_student = in_id_student AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_student
            USING HINT = 'student not exists';
    END IF;

    RETURN QUERY SELECT id_student, first_name, last_name, father_name, user_id, reg_date, mod_date
                 FROM students
                 WHERE id_student = in_id_student;




end;
$$
    LANGUAGE 'plpgsql';

