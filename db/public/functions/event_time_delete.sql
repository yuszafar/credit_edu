/*****************
* File name: event_time_delete.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION event_time_delete(in_id_event_time BIGINT DEFAULT NULL)
    returns VOID
as
$$
DECLARE
    FUNC_NAME TEXT := 'event_time_delete';
    ERR_CODE  INT  := 3002;

BEGIN
    IF in_id_event_time IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_event_time
            USING HINT = 'id_event_time cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM event_times WHERE id_event_time=in_id_event_time) OR
       EXISTS(SELECT 1 FROM event_times WHERE id_event_time = in_id_event_time AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_event_time
            USING HINT = 'event_time not exists';
    END IF;

    UPDATE event_times SET is_delete = TRUE, exp_date = now() WHERE id_event_time = in_id_event_time;




end;
$$
    LANGUAGE 'plpgsql';

