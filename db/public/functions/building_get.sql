/*****************
* File name: building_get.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION building_get(in_id_building BIGINT DEFAULT NULL)
    returns TABLE
            (
                out_id_building  BIGINT,
                out_name TEXT,
                out_address TEXT,
                out_size INT,
                out_reg_date  TIMESTAMP WITH TIME ZONE,
                out_mod_date  TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'building_get';
    ERR_CODE  INT  := 2004;

BEGIN
    IF in_id_building IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_building
            USING HINT = 'id_building cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM buildings WHERE id_building=in_id_building) OR
       EXISTS(SELECT 1 FROM buildings WHERE id_building = in_id_building AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_building
            USING HINT = 'building not exists';
    END IF;

    RETURN QUERY SELECT id_building, name, address, size, reg_date, mod_date
                 FROM buildings
                 WHERE id_building = in_id_building;




end;
$$
    LANGUAGE 'plpgsql';

