/*****************
* File name: event_time_add.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION event_time_add(in_number TEXT DEFAULT NULL, in_start_time TIME DEFAULT NULL, in_end_time TIME DEFAULT NULL)
    returns TABLE
            (
                out_id_event_time  BIGINT,
                out_number TEXT,
                out_start_time TIME,
                out_end_time TIME
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'event_time_add';
    ERR_CODE  INT  := 3001;

BEGIN
    IF in_number IS NULL OR in_start_time IS NULL OR in_end_time IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %', ERR_CODE, FUNC_NAME, in_number, in_start_time, in_end_time
            USING HINT = 'number or start_time or end_time cannot be null';
    END IF;


    RETURN QUERY INSERT INTO event_times (number, start_time, end_time) VALUES (in_number, in_start_time, in_end_time)
        RETURNING id_event_time, number, start_time, end_time;


end;
$$
    LANGUAGE 'plpgsql';

