/*****************
* File name: building_delete.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION building_delete(in_id_building BIGINT DEFAULT NULL)
    returns VOID
as
$$
DECLARE
    FUNC_NAME TEXT := 'building_delete';
    ERR_CODE  INT  := 2002;

BEGIN
    IF in_id_building IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_building
            USING HINT = 'id_building cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM buildings WHERE id_building=in_id_building) OR
       EXISTS(SELECT 1 FROM buildings WHERE id_building = in_id_building AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_building
            USING HINT = 'building not exists';
    END IF;

    UPDATE buildings SET is_delete = TRUE, exp_date = now() WHERE id_building = in_id_building;




end;
$$
    LANGUAGE 'plpgsql';

