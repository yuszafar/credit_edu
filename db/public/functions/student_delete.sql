/*****************
* File name: student_delete.sql
* Author: Zafar
* Date: 2021-05-05
* Version: 1.0
*************
* Changelog
* 2021-05-05 First init
*************/


CREATE OR REPLACE FUNCTION student_delete(in_id_student BIGINT DEFAULT NULL)
    returns VOID
as
$$
DECLARE
    FUNC_NAME TEXT := 'student_delete';
    ERR_CODE  INT  := 5002;
    deleted_account INT;

BEGIN
    IF in_id_student IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_student
            USING HINT = 'in_id_student cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM students WHERE id_student=in_id_student) OR
       EXISTS(SELECT 1 FROM students WHERE id_student = in_id_student AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %', ERR_CODE, FUNC_NAME, in_id_student
            USING HINT = 'manager not exists';
    END IF;

    SELECT user_id INTO deleted_account FROM students WHERE id_student = in_id_student;
    PERFORM account_delete(deleted_account);

    UPDATE students SET is_delete = TRUE, exp_date = now() WHERE id_student = in_id_student;




end;
$$
    LANGUAGE 'plpgsql';

