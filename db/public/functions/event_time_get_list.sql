/*****************
* File name: event_time_get_list.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
*************/


CREATE OR REPLACE FUNCTION event_time_get_list(in_id_event_time BIGINT DEFAULT NULL, in_number TEXT DEFAULT NULL,
                                               in_start_time TIME DEFAULT NULL, in_end_time TIME DEFAULT NULL)
    returns TABLE
            (
                out_id_event_time BIGINT,
                out_number        TEXT,
                out_start_time    TIME,
                out_end_time      TIME,
                out_reg_date      TIMESTAMP WITH TIME ZONE,
                out_mod_date      TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'event_time_get_list';
    ERR_CODE  INT  := 3005;

BEGIN
    return QUERY SELECT id_event_time,
                        number,
                        start_time,
                        end_time,
                        reg_date,
                        mod_date
                 FROM event_times

                 where (in_id_event_time IS NULL OR id_event_time = in_id_event_time)
                   AND (in_number IS NULL OR number = in_number)
                   AND (in_start_time IS NULL OR start_time = in_start_time)
                   AND (in_end_time IS NULL OR end_time = in_end_time)
                   AND is_delete = FALSE;

end;
$$
    LANGUAGE 'plpgsql';

