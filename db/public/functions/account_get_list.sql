/*****************
* File name: account_get_list.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-28 First init
* 2021-05-02 change return params
* 2021-05-02 added out_password
*************/


CREATE OR REPLACE FUNCTION account_get_list(in_account_id BIGINT DEFAULT NULL, in_username TEXT DEFAULT NULL)
    returns TABLE
            (
                out_id_user  BIGINT,
                out_username TEXT,
                out_password TEXT,
                out_reg_date TIMESTAMP WITH TIME ZONE,
                out_mod_date TIMESTAMP WITH TIME ZONE
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'account_get_list';
    ERR_CODE  INT  := 1005;

BEGIN
    return QUERY SELECT id_account,
                        username,
                        password,
                        reg_date,
                        mod_date
                 FROM accounts

                 where (in_account_id IS NULL OR id_account = in_account_id)
                   AND (in_username IS NULL OR username = in_username)
                   AND is_delete = FALSE;
end;
$$
    LANGUAGE 'plpgsql';

