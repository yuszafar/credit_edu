/*****************
* File name: manager_add.sql
* Author: Zafar
* Date: 2021-05-02
* Version: 1.0
*************
* Changelog
* 2021-05-02 First init
* 2021-05-05 add validation for in_account_id init
*************/


CREATE OR REPLACE FUNCTION manager_add(in_first_name TEXT DEFAULT NULL, in_last_name TEXT DEFAULT NULL,
                                       in_father_name TEXT DEFAULT NULL, in_account_id INT DEFAULT NULL)
    returns TABLE
            (
                out_id_manager  BIGINT,
                out_first_name  TEXT,
                out_last_name   TEXT,
                out_father_name TEXT,
                out_account_id  INT
            )
as
$$
DECLARE
    FUNC_NAME TEXT := 'manager_add';
    ERR_CODE  INT  := 4001;

BEGIN
    IF in_first_name IS NULL OR in_last_name IS NULL OR in_account_id IS NULL THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %', ERR_CODE, FUNC_NAME, in_first_name, in_last_name, in_father_name, in_account_id
            USING HINT = 'first_name or last_name or account_id cannot be null';
    END IF;

    IF NOT EXISTS(SELECT 1 FROM accounts WHERE id_account = in_account_id) OR
       EXISTS(SELECT 1 FROM accounts WHERE id_account = in_account_id AND is_delete = TRUE) THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %', ERR_CODE, FUNC_NAME, in_first_name, in_last_name, in_father_name, in_account_id
            USING HINT = 'account not exists';
    END IF;

    IF EXISTS(SELECT 1 FROM managers WHERE user_id = in_account_id) OR
       EXISTS(SELECT 1 FROM students WHERE user_id = in_account_id) OR
       EXISTS(SELECT 1 FROM teachers WHERE user_id = in_account_id) THEN
        RAISE EXCEPTION 'Error --> % % params %, %, %, %', ERR_CODE, FUNC_NAME, in_first_name, in_last_name, in_father_name, in_account_id
            USING HINT = 'a manager with the account_id already exists';
    END IF;

    RETURN QUERY INSERT INTO managers (first_name, last_name, father_name, user_id) VALUES (in_first_name, in_last_name,
                                                                                            in_father_name, in_account_id)
        RETURNING id_manager, first_name, last_name, father_name, user_id;


end;
$$
    LANGUAGE 'plpgsql';

