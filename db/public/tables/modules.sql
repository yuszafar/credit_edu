/*****************
* File name: modules.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-27 First init
*************/

CREATE TABLE IF NOT EXISTS modules (
    id_module BIGSERIAL PRIMARY KEY,
    size INT,
    size_max INT,

    subject_id INT NOT NULL,
    teacher_id INT NOT NULL,
    is_delete BOOLEAN default FALSE

);

ALTER TABLE modules
    ADD COLUMN IF NOT EXISTS size INT,
    ADD COLUMN IF NOT EXISTS size_max INT,
    ADD COLUMN IF NOT EXISTS subject_id INT,
    ADD COLUMN IF NOT EXISTS teacher_id INT,

    ADD COLUMN IF NOT EXISTS reg_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
    ADD COLUMN IF NOT EXISTS mod_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS exp_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS is_delete BOOLEAN NOT NULL DEFAULT FALSE;