/*****************
* File name: schedules.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-27 First init
*************/


CREATE TABLE IF NOT EXISTS schedules (
    id_schedule BIGSERIAL PRIMARY KEY,
    day DATE NOT NULL,
    module_id INT NOT NULL,
    event_time_id INT NOT NULL,
    room_id INT NOT NULL,

    is_delete BOOLEAN DEFAULT FALSE
);

ALTER TABLE schedules
    ADD COLUMN IF NOT EXISTS day DATE,
    ADD COLUMN IF NOT EXISTS module_id INT,
    ADD COLUMN IF NOT EXISTS event_time_id INT,
    ADD COLUMN IF NOT EXISTS room_id INT,

    ADD COLUMN IF NOT EXISTS reg_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
    ADD COLUMN IF NOT EXISTS mod_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS exp_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS is_delete BOOLEAN NOT NULL DEFAULT FALSE;