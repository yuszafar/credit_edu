/*****************
* File name: module_student_applications.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-27 First init
*************/

CREATE TABLE IF NOT EXISTS module_student_applications(
    id_module_student_application BIGSERIAL PRIMARY KEY,
    date_apply TIMESTAMP WITH TIME ZONE,
    date_approved TIMESTAMP WITH TIME ZONE,
    date_rejected TIMESTAMP WITH TIME ZONE,

    module_id INT NOT NULL,
    student_id INT NOT NULL,
    is_delete BOOLEAN default FALSE
);

ALTER TABLE module_student_applications
    ADD COLUMN IF NOT EXISTS date_apply TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS date_approved TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS date_rejected TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS module_id INT,
    ADD COLUMN IF NOT EXISTS student_id INT,

    ADD COLUMN IF NOT EXISTS reg_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
    ADD COLUMN IF NOT EXISTS mod_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS exp_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS is_delete BOOLEAN NOT NULL DEFAULT FALSE;