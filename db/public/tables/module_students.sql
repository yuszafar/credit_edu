/*****************
* File name: module_students.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-27 First init
*************/


CREATE TABLE IF NOT EXISTS module_students(
    id_module_student BIGSERIAL PRIMARY KEY,
    module_id INT NOT NULL,
    student_id INT NOT NULL,
    is_delete BOOLEAN default FALSE
);


ALTER TABLE module_students
    ADD COLUMN IF NOT EXISTS module_id INT,
    ADD COLUMN IF NOT EXISTS student_id INT,

    ADD COLUMN IF NOT EXISTS reg_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
    ADD COLUMN IF NOT EXISTS mod_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS exp_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS is_delete BOOLEAN NOT NULL DEFAULT FALSE;