/*****************
* File name: event_times.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-27 First init
*************/


CREATE TABLE IF NOT EXISTS event_times (
    id_event_time BIGSERIAL PRIMARY KEY,
    number TEXT NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,

    is_delete BOOLEAN default FALSE
);


ALTER TABLE event_times
    ADD COLUMN IF NOT EXISTS number TEXT,
    ADD COLUMN IF NOT EXISTS start_time TIME,
    ADD COLUMN IF NOT EXISTS end_time TIME,

    ADD COLUMN IF NOT EXISTS reg_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
    ADD COLUMN IF NOT EXISTS mod_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS exp_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS is_delete BOOLEAN NOT NULL DEFAULT FALSE;