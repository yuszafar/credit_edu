/*****************
* File name: subjects.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-27 First init
*************/

CREATE TABLE IF NOT EXISTS subjects (
    id_subject BIGSERIAL PRIMARY KEY,
    name TEXT NOT null,

    is_delete BOOLEAN default FALSE
);

ALTER TABLE subjects
    ADD COLUMN IF NOT EXISTS name TEXT,

    ADD COLUMN IF NOT EXISTS reg_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
    ADD COLUMN IF NOT EXISTS mod_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS exp_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS is_delete BOOLEAN NOT NULL DEFAULT FALSE;