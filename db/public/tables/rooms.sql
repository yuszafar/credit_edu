/*****************
* File name: rooms.sql
* Author: Zafar
* Date: 2021-04-27
* Version: 1.0
*************
* Changelog
* 2021-04-27 First init
*************/


CREATE TABLE IF NOT EXISTS rooms (
    id_room BIGSERIAL PRIMARY KEY,
    number TEXT NOT NULL,
    size INT NOT NULL,
    building_id INT NOT NULL,

    is_delete BOOLEAN DEFAULT FALSE
);

ALTER TABLE rooms
    ADD COLUMN IF NOT EXISTS number TEXT,
    ADD COLUMN IF NOT EXISTS size INT,
    ADD COLUMN IF NOT EXISTS building_id INT,

    ADD COLUMN IF NOT EXISTS reg_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
    ADD COLUMN IF NOT EXISTS mod_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS exp_date TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS is_delete BOOLEAN NOT NULL DEFAULT FALSE;